// -*- coding: utf-8 -*-

'use strict';

(function () {
    var url = document.querySelector('a[rel="next"]').href;
    var req = new XMLHttpRequest();
    req.open('GET', url, true);
    req.responseType = 'document';
    req.addEventListener('load', function (e) {
        var doc = e.target.response;
        var message = [
            'doc.defaultView = ' + doc.defaultView,
            'doc.URL         = ' + doc.URL,
            'doc.baseURI     = ' + doc.baseURI,
            'doc.documentURI = ' + doc.documentURI,
            'doc.domain      = ' + doc.domain,
            'doc.location    = ' + doc.location
        ].join(',\n');
        console.log(message);
    }, false);
    req.send(null);
}());

/*
 * Environment:
 *   UA: Mozilla/5.0 (X11; Linux i686; rv:11.0a2) Gecko/20120131 Firefox/11.0a2
 *   Add-on SDK: 1.5b3
 * Result (in Web Console):
 *   doc.defaultView = null,
 *   doc.URL         = http://misc-xkyrgyzstan.dotcloud.com/aptests/redirect/ng/cors,
 *   doc.baseURI     = http://misc-xkyrgyzstan.dotcloud.com/aptests/redirect/ng/cors,
 *   doc.documentURI = http://misc-xkyrgyzstan.dotcloud.com/aptests/redirect/ng/cors,
 *   doc.domain      = misc-xkyrgyzstan.dotcloud.com,
 *   doc.location    = null
 * Result (in Add-on):
 *    doc.defaultView = null,
 *    doc.URL         = http://misc-xkyrgyzstan.dotcloud.com/aptests/redirect/ng/cors,
 *    doc.baseURI     = http://misc-xkyrgyzstan.dotcloud.com/aptests/redirect/ng/cors,
 *    doc.documentURI = http://misc-xkyrgyzstan.dotcloud.com/aptests/redirect/ng/cors,
 *    doc.domain      = misc-xkyrgyzstan.dotcloud.com,
 *    doc.location    = null
 */
